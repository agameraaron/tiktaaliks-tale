extends Node2D


var input_disabled = false
var cursor_selection = 0
var sound_volume_test_delay = 0

func _ready():
	get_node("menu sounds").set_volume_db(linear2db(get_parent().sound_volume))
	get_parent().next_song = "Kevin Macleod - Chill Wave"
	

func _process(delta):
	if input_disabled == false:
		if Input.is_action_just_pressed("up"):
			cursor_selection += -1
		elif Input.is_action_just_pressed("down"):
			cursor_selection += 1
		
		if cursor_selection > 3:
			cursor_selection = 0
		elif cursor_selection < 0:
			cursor_selection = 3
		
		#sound volume
		if cursor_selection == 0:
			if Input.is_action_just_pressed("circle"):
				get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
				get_node("menu sounds").play()
			
			if Input.is_action_pressed("left"):
				
				#Hack:  A workaround to set the volume at an effective zero because Godot
				#    starts cutting out *all* audio busses when either the streamplayer or
				#    audioserver's volume hits a linear zero.
				if get_parent().sound_volume > 0.01:
					get_parent().sound_volume += -0.01
					#This method updates waaay too slow. Instead set the stream player volume.
					#AudioServer.set_bus_volume_db(2,linear2db(get_parent().sound_volume))
					get_node("menu sounds").set_volume_db(linear2db(get_parent().sound_volume))
				else:  
					get_node("menu sounds").set_volume_db(linear2db(0.00001))
				
				if sound_volume_test_delay > 0:
					sound_volume_test_delay += -1
				else:
					get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
					get_node("menu sounds").play()
					sound_volume_test_delay = 6
			elif Input.is_action_just_released("left"): #so it instantly plays when pressed again
				sound_volume_test_delay = 0
			elif Input.is_action_pressed("right"):
				
				if get_parent().sound_volume < 1:
					get_parent().sound_volume += 0.01
					get_node("menu sounds").set_volume_db(linear2db(get_parent().sound_volume))
				
				if sound_volume_test_delay > 0:
					sound_volume_test_delay += -1
				else:
					get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
					get_node("menu sounds").play()
					sound_volume_test_delay = 6
			elif Input.is_action_just_released("right"): #so it instantly plays when pressed again
				sound_volume_test_delay = 0
			
		
		#music volume
		elif cursor_selection == 1:
			if Input.is_action_pressed("left"):
				
				#Hack:  A workaround to set the volume at an effective zero because Godot
				#    starts cutting out *all* audio busses when either the streamplayer or
				#    audioserver's volume hits a linear zero.
				if get_parent().music_volume > 0.01:
					get_parent().music_volume += -0.01
					#This method updates waaay too slow. Instead set the stream player volume.
					#AudioServer.set_bus_volume_db(2,linear2db(get_parent().music_volume))
					get_node("/root/program manager/music player").set_volume_db(linear2db(get_parent().music_volume))
				else:  
					get_node("/root/program manager/music player").set_volume_db(linear2db(0.00001))
				
#				if get_parent().music_volume > 0:
#					get_parent().music_volume += -0.01
#					get_node("/root/program manager/music player").set_volume_db(linear2db(get_parent().music_volume))
				
				
			elif Input.is_action_pressed("right"):
				
				if get_parent().music_volume < 1:
					get_parent().music_volume += 0.01
					get_node("/root/program manager/music player").set_volume_db(linear2db(get_parent().music_volume))
				
			
		
		#fullscreen
		elif cursor_selection == 2 and (Input.is_action_just_pressed("start") or Input.is_action_just_pressed("circle")):
			get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
			get_node("menu sounds").play()
			if OS.is_window_fullscreen() == true:
				OS.set_window_fullscreen(false)
				OS.set_window_maximized(true)
				get_node("fullscreen checkmark").hide()
			else:
				OS.set_window_fullscreen(true)
				get_node("fullscreen checkmark").show()
		
		#back to title
		elif cursor_selection == 3 and (Input.is_action_just_pressed("start") or Input.is_action_just_pressed("circle")):
			get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
			get_node("menu sounds").play()
			get_parent().next_mode = "title"
			get_parent().fadeout_music_active = true
			input_disabled = true
		
		
		
		get_node("options/cursor").set_position(Vector2(40,64+(20*cursor_selection)))
		
		get_node("sound slider").set_position(Vector2(179+(get_parent().sound_volume*100),58))
		get_node("music slider").set_position(Vector2(179+(get_parent().music_volume*100),78))
