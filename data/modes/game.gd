extends Node2D

var game_state = ""
var top_stage = "stage 1"
var current_stage = "tutorial"
var next_stage = "tutorial"
var next_stage_cutscene = false
var fadeout_active = 0
var instantly_fadeout = false

var stages = ["tutorial","stage 1","stage 2","stage boss"]

#player data memory
var player_health = 10


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(_delta):
	
	if game_state == "win":
		
		next_stage = "win"
		#get_parent().get_node("music player").stop()
		if Input.is_action_just_pressed("start"):
			game_state = ""
			get_parent().next_mode = "title"
			get_parent().fadeout_music_active = true
		#play ending music
	if game_state == "lose":
		get_parent().next_song = ""
		next_stage = "lose"
		get_node("/root/program manager/mode/player interface/health bar").hide()
		
		if Input.is_action_just_pressed("start"):
			game_state = ""
			#get_parent().current_mode = ""
			#print("top stage: "+str(top_stage))
			next_stage = top_stage
			
	if next_stage != current_stage:
		
		if instantly_fadeout == true:
			instant_fadeout()
			fadeout_active = 22
			instantly_fadeout = false
			
		
		if fadeout_active > 1:
			fadeout_active += -1
		elif fadeout_active <= 0:
			fadeout()
		else:
			free_stage()
			load_new_stage()
			
			#if not game over screen
			
			
			
			fadein()
			
			if next_stage != "lose" and next_stage_cutscene == false:
				get_node("/root/program manager/mode/player interface/health bar").show()
				get_node("/root/program manager/mode/stage/sprites/player").health = 10
				get_node("/root/program manager/mode/player interface/health bar").set_value(10)
				#print("player health: "+str(player_health))
			if next_stage == "stage 1":
				get_parent().next_song = "Gravity Sound - Soundscape Dark"
			elif next_stage == "stage 2":
				get_parent().next_song = "Gravity Sound - Soundscape Dark"
			current_stage = next_stage
			fadeout_active = 0
			
			
	if get_parent().debug == true:
		debug_tools()

func debug_tools():
	if Input.is_action_just_pressed("debug next level"):
		var stage_number = stages.find(current_stage)
		if stage_number >= 0: #if valid entry
			next_stage = stages[stage_number+1]
			top_stage = next_stage
		

func load_new_stage():
	var new_stage = load("res://data/stages/"+next_stage+".tscn").instance()
	add_child(new_stage)
	
	

func instant_fadeout():
	get_parent().get_node("proscenium/curtain").set_frame_color(Color(0,0,0,255))

func fadeout():
	fadeout_active = 100
	get_parent().get_node("proscenium/transition player").play("fade out")

func fadein():
	get_parent().get_node("proscenium/transition player").play("fade in")
	
func free_stage():
	get_node("stage").free()
