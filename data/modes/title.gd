extends Node2D


var input_disabled = false
var menu_displayed = false
var cursor_selection = 0


func _ready():
	get_node("menu sounds").set_volume_db(linear2db(get_parent().sound_volume))
	
	get_node("begin label/begin flashing animation").play("begin")
	get_node("version label").set_text(str(get_parent().program_version))

func _process(delta):
	
	if menu_displayed == true and input_disabled == false:
		
		#todo menu management
		if Input.is_action_just_pressed("up"):
			cursor_selection += -1
		elif Input.is_action_just_pressed("down"):
			cursor_selection += 1
		
		if cursor_selection > 3:
			cursor_selection = 0
		elif cursor_selection < 0:
			cursor_selection = 3
		
		#start game
		if cursor_selection == 0 and (Input.is_action_just_pressed("start") or Input.is_action_just_pressed("circle")):
			get_parent().next_mode = "game"
			get_parent().fadeout_music_active = true
			get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
			get_node("menu sounds").play()
			input_disabled = true
		
		#options
		elif cursor_selection == 1 and (Input.is_action_just_pressed("start") or Input.is_action_just_pressed("circle")):
			get_parent().next_mode = "options"
			get_parent().fadeout_music_active = true
			get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
			get_node("menu sounds").play()
			input_disabled = true
		
		#credits
		elif cursor_selection == 2 and (Input.is_action_just_pressed("start") or Input.is_action_just_pressed("circle")):
			get_parent().next_mode = "credits"
			get_parent().fadeout_music_active = true
			get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
			get_node("menu sounds").play()
			input_disabled = true
		
		#quit
		elif cursor_selection == 3 and (Input.is_action_just_pressed("start") or Input.is_action_just_pressed("circle")):
			get_tree().quit()
		
		get_node("options/cursor").set_position(Vector2(96,109+(20*cursor_selection)))
		
	elif menu_displayed == false:
		
		if get_node("begin label/begin flashing animation").get_current_animation() == "begin":
			pass
		elif get_node("begin label/begin flashing animation").get_current_animation() == "begin loop":
			pass
		else:
			get_node("begin label/begin flashing animation").play("begin loop")
		
		#display menu
		if Input.is_action_just_pressed("start"):
			menu_displayed = true
			get_node("options").show()
			get_node("begin label/begin flashing animation").stop()
			get_node("begin label").hide()
