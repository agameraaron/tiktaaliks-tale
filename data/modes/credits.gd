extends Node2D

var input_disabled = false

func _ready():
	get_node("menu sounds").set_volume_db(linear2db(get_parent().sound_volume))
	get_node("credits").set_text(str(get_parent().program_version)+" \""+str(get_parent().program_version_name)+"\"\n\nProgrammer:\nAaron Meachum\n\nVisual artist:\nBojidar Marinov\n\nMusicians:\nKevin Macleod\nGravity Sound\n\nCreated for the\n1st Godot Wild Jam.")

func _process(delta):
	if Input.is_action_just_pressed("start") and input_disabled == false:
		get_parent().next_mode = "title"
		#get_node("menu sounds").set_stream(load("res://assets/sounds/confirmdialogue2.wav"))
		#get_node("menu sounds").play()
		input_disabled = true
