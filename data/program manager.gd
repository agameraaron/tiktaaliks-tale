extends Node

var debug = true

var program_version = "v0.3"
var program_version_name = "Contest"

var current_mode = "title"
var next_mode = "title"

var sound_volume = 0.5
var music_volume = 0.7
var fadeout_music_active = false
var current_song = ""
var next_song = ""

var fadeout_active = 0

func _ready():
	
	get_node("music player").set_volume_db(linear2db(music_volume))
	
	if debug == true:
		pass
	else:
		OS.set_window_maximized(true)
	#fadein()

func _process(delta):
	if Input.is_action_just_pressed("select"):
		if current_mode == "title":
			get_tree().quit()
		else:
			fadeout_active = 1 #skip transition
			instant_fadeout()
			next_mode = "title"
			next_song = ""
	if next_mode != current_mode:
		#free everything under program manager aside from exceptions
		
		
		#instant_fadeout()
		if fadeout_active > 1:
			fadeout_active += -1
		elif fadeout_active == 1:
			free_mode()
			load_next_mode()
			fadein()
			fadeout_active = 0
		else:
			fadeout()
	
	if current_song != next_song:
		if next_song == "":
			get_node("music player").stop()
		else:
			get_node("music player").set_stream(load("res://assets/music/"+next_song+".ogg"))
			get_node("music player").play()
		current_song = next_song
	
	
	if fadeout_music_active == true:
		var cur_music_volume = db2linear(get_node("music player").get_volume_db())
		if cur_music_volume > 0.03:
			
			get_node("music player").set_volume_db(linear2db(cur_music_volume - 0.02))
		else:
			get_node("music player").set_volume_db(linear2db(0.001))
			get_node("music player").stop()
			fadeout_music_active = false

func fadein():
	get_node("proscenium/transition player").play("fade in")
	get_node("music player").set_volume_db(linear2db(music_volume))

func fadeout():
	get_node("proscenium/transition player").play("fade out")
	fadeout_active = 52
	
	

func instant_fadeout():
	get_node("proscenium/curtain").set_frame_color(Color(0,0,0,255))

func load_next_mode():
	var mode_load = load("res://data/modes/"+str(next_mode)+".tscn").instance()
	add_child(mode_load)
	current_mode = next_mode


func free_mode():
	for child_counter in range(0,self.get_child_count()):
		#exceptions
		if get_child(child_counter).get_name() == "mode":
			get_child(child_counter).free()
