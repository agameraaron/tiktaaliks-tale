extends Node2D

var timer = 180
var go_to_stage = "stage 2"
var top_stage = "stage 2"

func _ready():
	pass

func _process(_delta):
	if timer > 0:
		timer += -1
	else:
		get_parent().next_stage = go_to_stage
		get_parent().top_stage = top_stage
		get_parent().next_stage_cutscene = false
		
