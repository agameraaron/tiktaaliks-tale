extends KinematicBody2D

var contact_damage = 6

var activate = false
var sine_reverse = true
var sine_value = 0
var command_velocity = Vector2(0,0)
var sine_velocity = Vector2(0,0)


func _ready():
	
	sine_value = (randi() % 2)
	#print("spawn initial sine value: "+str(sine_value))
	sine_reverse = (randi() % 2)
	#print("spawn initial sine reverse: "+str(sine_value))

func _process(delta):
	
	damage()
	
	var bodies_seen = get_node("sight").get_overlapping_bodies()
	for overlapping_body in range(0,bodies_seen.size()):
		if bodies_seen[overlapping_body].get_name() == "player":
			activate = true
	
	if activate == true:
		
		sine_value += -0.024
		
		
		command_velocity.x = -32
		if sine_reverse == 1:
			sine_velocity.y = ((sin(sine_value)*45))
		elif sine_reverse == 0:
			sine_velocity.y = ((sin(sine_value*(-1))*45))
		
		
		#print(sine_value)

func _physics_process(delta):
		move_and_slide(Vector2(command_velocity.x,sine_velocity.y)*65*delta)
		
		#set_position(Vector2(get_position().x,sine_velocity.y*65*delta))



func damage():
	var overlapping_bodies = get_node("hurt collider").get_overlapping_bodies()
	for overlapping_body in range(0,overlapping_bodies.size()):
		if overlapping_bodies[overlapping_body].get_name() == "player" and overlapping_bodies[overlapping_body].invincibility_time <= 0:
			overlapping_bodies[overlapping_body].health -= contact_damage
			
			#find difference between two points
			var projected_knockback_direction = ((get_node("hurt collider").position - overlapping_bodies[overlapping_body].position).normalized())
			
			#print("projected knockback direction difference between two points: "+str(projected_knockback_direction))
			overlapping_bodies[overlapping_body].knockback_direction = projected_knockback_direction
			
			overlapping_bodies[overlapping_body].accept_damage()
