extends KinematicBody2D

export var leftward_force = 15

const gravity = 2500
var land_gravity_cap = 13000
var jump_force = 20000
var is_jumping = false
var jump_time = 0
var jump_time_max = 180
var jump_time_buffer = 0
var player_seen = false
var command_velocity_deceleration = 0
var command_velocity = Vector2()
var physics_velocity = Vector2()
var descent_force = 220

#phase intelligence management
var phase_time_left = 180

#intelligence
var attack_phase = 0

func _ready():
	pass

func _process(delta):
	
	#reset physics
	#physics_velocity = Vector2(0,0)
	
	damage()
	
	if is_jumping:
		jumping()
	
	if attack_phase == 0: #little hop preceding dash
		if phase_time_left == 30:
			$sprite.flip_h = true
		if phase_time_left > 0: #wait a bit
			phase_time_left -= 1
		else: #then do a little hop
			jump(0)
			attack_phase = 1
			phase_time_left = 90
	elif attack_phase == 1: #wait before dash
		if phase_time_left > 0:
			phase_time_left -= 1
		else:
			phase_time_left = 90
			attack_phase = 2
	elif attack_phase == 2: #dash toward left
		command_velocity.x = -6500
		if phase_time_left > 0:
			phase_time_left -= 1
		else:
			phase_time_left = 60
			attack_phase = 3
			command_velocity.x = 0
	elif attack_phase == 3: #turn right and wait a second
		if phase_time_left == 30:
			$sprite.flip_h = false
		
		if phase_time_left > 0:
			phase_time_left -= 1
		else:
			phase_time_left = 150
			attack_phase = 4
	elif attack_phase == 4: #Bouncing jump right toward player
		command_velocity.x = 3000
		
		if phase_time_left > 0:
			phase_time_left -= 1
		else:
			phase_time_left = 240
			attack_phase = 5
	
	
	"""
	var overlapping_bodies = get_node("sight").get_overlapping_bodies()
	for overlapping_body in range(0,overlapping_bodies.size()):
		if overlapping_bodies[overlapping_body].get_name() == "player":
			player_seen = true
			var player_body = overlapping_bodies[overlapping_body]
			if player_body.jump_initiated > 0: #player is jumping while in range, initialize jump
				if jumping != true:
					jump()
					
	
	if player_seen == true:
		#move left
		command_velocity.x = -leftward_force
	"""
	
	#gravity
	if physics_velocity.y < land_gravity_cap:
		physics_velocity.y += gravity
	
	

func _physics_process(delta):
	move_and_slide((command_velocity+physics_velocity)*delta,Vector2(0,-1))
	

func damage():
	var overlapping_bodies = get_node("hurt collider").get_overlapping_bodies()
	for overlapping_body in range(0,overlapping_bodies.size()):
		if overlapping_bodies[overlapping_body].get_name() == "player" and overlapping_bodies[overlapping_body].invincibility_time <= 0:
			overlapping_bodies[overlapping_body].health -= 1
			overlapping_bodies[overlapping_body].invincibility_time = 90

func jump(how_high):
	
	is_jumping = true
	if how_high == 0: #short hop
		jump_time = 60 #???
		jump_force = 20000
	elif how_high == 1: #hop
		jump_time = 90 
		jump_force = 20000
	elif how_high == 2:
		jump_time = 90
		jump_force = 32000
	elif how_high == 3:
		jump_time = 120
		jump_force = 32000
	#print("jump!: "+str(jump_time))
	jump_time_buffer = jump_time - 7
	

func jumping():
	
	#print("currently jumping...")
	if jump_time < jump_time_buffer: #able to test if is on floor again
		if is_on_floor():
			print("Landed")
			is_jumping = false
			command_velocity_deceleration = 0
			command_velocity.y = 0
		else:
			command_velocity.y += descent_force #???
	elif jump_time > 0: #decrease jump time
		jump_time -= 1
		command_velocity.y = -jump_force
		#command_velocity_deceleration += gravity #increase deceleration
		#command_velocity.y += command_velocity_deceleration
	
	
	command_velocity_deceleration += 0.01 #increase deceleration
	command_velocity.y += command_velocity_deceleration
		
		
		
		
		
