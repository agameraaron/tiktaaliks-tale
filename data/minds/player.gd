extends KinematicBody2D


#state machines
#active state
enum active_states_available{ACTIVE,STUN}
var active_state = active_states_available.ACTIVE

#damage reception
var health = 10
var interface_health = health
var invincibility_time = 0
var knockback_direction = Vector2(0,0)
var knockback_rate = 4200
var knockback_time = 0

var land_gravity = 2500
var land_gravity_cap = 13000
var horizontal_walk_speed = 8000
var water_gravity = 250
var water_gravity_cap = 800
var horizontal_swim_speed = 7000
var vertical_swim_speed = 8500
var jump_force = 20000
var descent_force = 220

var environment_state = "land"
var physics_velocity = Vector2(0,0)
var command_velocity = Vector2(0,0)



var jump_available = 0
var jump_initiated = 0
var jump_time_available = 0
var jump_time_max = 30
var jump_descending = 1
var jump_cooldown = 0
var jump_cooldown_max = 46



func accept_damage():
	invincibility_time = 135 #150% as long as the knockback_time
	knockback_time = 90
	active_state = active_states_available.STUN
	#set these up
	physics_velocity = Vector2(0,0)
	command_velocity = Vector2(0,0)

func _ready():
	get_node("sound player").set_volume_db(linear2db(get_node("/root/program manager").sound_volume))
	#get_node("/root/program manager/mode/player interface/health").set_text(str(health))
	#update player health
	health = get_node("/root/program manager/mode").player_health
	#get_node("stage/sprites/player").health = player_health

func _input(_event):
	pass
	
func _process(delta):
	
	if invincibility_time > 0:
		invincibility_time += -1
	
	#update interface & player data
	if health != interface_health:
		#get_node("/root/program manager/mode/player interface/health").set_text(str(health))
		interface_health = health
		get_node("/root/program manager/mode/player interface/health bar").set_value(health)
		get_node("/root/program manager/mode").player_health = health #update game manager for respawning with the same stats
		print("knockback direction: "+str(knockback_direction))
	
	if health <= 0 and get_node("/root/program manager/mode").game_state != "lose":
		#get_node("/root/program manager/mode/player interface/health").set_text("")
		get_node("/root/program manager/mode/player interface/health bar").hide()
		get_node("/root/program manager/mode").game_state = "lose"
		get_node("/root/program manager/mode").instantly_fadeout = true
		
		get_node("sound player").set_stream(load("res://assets/sounds/big hurts.wav"))
		get_node("sound player").play()
	#command_velocity = Vector2(0,0)
	if environment_state == "land":
		
		if active_state == active_states_available.ACTIVE:
			if is_on_floor():
				jump_available = 1
				jump_descending = 0
				jump_initiated = 0
			
			if jump_initiated > 0:
				jump_initiated += -1
				#for AI observance of player jumping
				
			if jump_descending and command_velocity.y < 0:
				command_velocity.y += descent_force
				#print("descending: "+str(command_velocity.y))
			
			if jump_available == 1:
				#if command_velocity.y < 0: #still going upwards, more rapidly descend
					#command_velocity.y += 0.1
				if Input.is_action_pressed("circle") and jump_time_available >= 0 and jump_descending == 0: #swim/jump
					jump_initiated = 4
					command_velocity.y = -jump_force
					#jump_cooldown = jump_cooldown_max
					jump_time_available += -1
				else:
					jump_available = 0
					jump_descending = 1
					jump_time_available = jump_time_max
			
			if Input.is_action_pressed("left"):
				command_velocity.x = -horizontal_walk_speed
				get_node("sprite").set_flip_h(true)
				if $"animator".get_current_animation() != "walk":
					$"animator".play("walk")
			elif Input.is_action_pressed("right"):
				command_velocity.x = horizontal_walk_speed
				get_node("sprite").set_flip_h(false)
				if $"animator".get_current_animation() != "walk":
					$"animator".play("walk")
			else:
				command_velocity.x = 0
				if $"animator".get_current_animation() != "walk idle":
					$"animator".play("walk idle")
			
		if physics_velocity.y < land_gravity_cap: #gravity
			physics_velocity.y += land_gravity
	
	elif environment_state == "water":
		if active_state == active_states_available.ACTIVE:
			if $"animator".get_current_animation() != "swim":
				$"animator".play("swim")
			
			#velocity.x = 0
			if Input.is_action_pressed("up"):
				command_velocity.y = -vertical_swim_speed
			elif Input.is_action_pressed("down"):
				command_velocity.y = vertical_swim_speed+10
			else:
				command_velocity.y = 0
			if Input.is_action_pressed("left"):
				command_velocity.x = -horizontal_swim_speed
			elif Input.is_action_pressed("right"):
				command_velocity.x = horizontal_swim_speed
			else:
				command_velocity.x = 0
			#reset momentum
			#velocity.x = 0
			
		if physics_velocity.y < water_gravity_cap: #gravity
			physics_velocity.y += water_gravity
	
	knockback()

func knockback():
	if abs(knockback_direction.x) > 0 or abs(knockback_direction.y) > 0:
		knockback_time -= 1
		if knockback_time <= 0: #time up
			knockback_direction = Vector2(0,0) #reset
			active_state = active_states_available.ACTIVE

func _physics_process(delta):
	move_and_slide((physics_velocity+command_velocity+(knockback_direction*knockback_rate))*delta,Vector2(0,-1))#.set_position(Vector2(get_position().x,get_position().y + gravity))
