![](https://raw.githubusercontent.com/agameraaron/tiktaaliks-tale/master/showcase/banner.png)

Play as a handsome tiktaalik in this short platforming adventure game!

Created for the 1st week-long Godot Wild Jam.

### About:

Version 0.2.2 alpha, "Post-Jam Refinement"

MIT licensed. Created by "AGamerAaron".

Created & editable in Godot Engine v3.0: https://godotengine.org/

For downloads or more information, visit the itch.io page:

https://agameraaron.itch.io/tiktaaliks-tale

### Screenshots:

![Level 1](https://raw.githubusercontent.com/agameraaron/tiktaaliks-tale/master/showcase/gameplay.png)

![Level 2](https://raw.githubusercontent.com/agameraaron/tiktaaliks-tale/master/showcase/gameplay%202.png)

### Credits:

Programming: AGamerAaron

Artist: Bojidar Marinov